from math import log, sqrt, pi, exp
from scipy.stats import norm
import numpy as np
import scipy.stats as si


def implied_vol_outer(option_type: str, option_price: float, S: float, K: float, T: int, r: float, q: float, call_fun: object, put_fun: object, precision: float = 0.0001)->float:
    # apply bisection method to get the implied volatility by solving the BSM function
    assert option_type == 'call' or option_type == 'put', 'option_type should be call or put'
    # precision = 0.00001
    upper_vol = 50.0
    max_vol = 50.0
    min_vol = 0.0001
    lower_vol = 0.0001
    iteration = 0

    while True:
        iteration += 1
        mid_vol = (upper_vol + lower_vol)/2.0
        if option_type == 'call':
            price = call_fun(S, K, T, r, mid_vol, q)
            lower_price = call_fun(S, K, T, r, lower_vol, q)
            if (lower_price - option_price) * (price - option_price) > 0:
                lower_vol = mid_vol
            else:
                upper_vol = mid_vol
            if abs(price - option_price) < precision:
                break
            if mid_vol > max_vol - 5:
                mid_vol = 0.0001
                break
        elif option_type == 'put':
            price = put_fun(S, K, T, r, mid_vol, q)
            upper_price = put_fun(S, K, T, r, upper_vol, q)
            if (upper_price - option_price) * (price - option_price) > 0:
                upper_vol = mid_vol
            else:
                lower_vol = mid_vol
            if abs(price - option_price) < precision:
                break
            if iteration > 100:
                break
    return mid_vol


if __name__ == '__main__':
    from black_scholes import BlackScholes

    S = 100.0         # Spot Price
    K = 100.0      # Strike Price
    r = 0.05      # risk Free Rate
    q = 0.0      # Dividend
    v = 0.20                        # Volatility
    T = 1     # Years to maturity
    PutCall = 'call'
    OpStyle = 'E'

    bs = BlackScholes()
    # print('Black Scholes results:')
    print('price %.4f' % bs.calculate_value(
        S, K, T, r, v, q, option_type=PutCall))
    # print('calculated greeks are ')
    # print(bs.print_greeks(S, K, T, r, v, q, PutCall, OpStyle))
    print('implied vol: %.4f' % implied_vol_outer(
        PutCall, 5.57, S, K, T, r, q, bs.bs_call, bs.bs_put))
    from trinomial_tree import TrinomialTree, trinomial_price
    print('Trinomial Tree results:')
    tt = TrinomialTree(max_inter=3000, precision=0.0001)
    value = tt.calculate_value(S, K, T, r, v, q, PutCall, OpStyle)
    print('calculated value is %.4f' % value)
    # print('calculated greeks are')
    # print(tt.print_greeks(S, K, T, r, v, q, PutCall, OpStyle))

    print('implied vol: %.4f' % implied_vol_outer(
        PutCall, 5.57, S, K, T, r, q, tt.tt_call, tt.tt_put, precision=0.0001))
