This branch mainly solves the following problems, whose solutions are presented in the .tex file.

1.Given the daily asset value of portfolio A and B (portfolio.csv), calculate the normalized annual return, annual volatility, max drawdown, sharp ratio, sortino ratio, VaR of each portfolio. Analyze the relation between two portfolios, using at least three methods.

2.Give the hdf5 file, please load the data from the file (sample.h5) and store in a pandas DataFrame.

3.Write function/class to calculate the vanilla option price and greeks (delta, gamma, rho, vega, etc.), using Black-Scholes and Trinomial-Tree models. Also write function/class to calculate the implied volatility using Black-Scholes and Trinomial-Tree models.

4.Write a TCP client and host class, which send heart beat messages to each other. The host shall allows multiple client connections simultaneous. The client and host shall be properly constructed and closed, and managed by different processes.
