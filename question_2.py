import h5py
import pandas as pd
import numpy as np


def traverse_datasets(hdf_file):

    def h5py_dataset_iterator(g, prefix=''):
        for key in g.keys():
            item = g[key]
            path = f'{prefix}/{key}'
            if isinstance(item, h5py.Dataset):  # test for dataset
                yield (path, item)
            elif isinstance(item, h5py.Group):  # test for group (go down)
                yield from h5py_dataset_iterator(item, path)

    for path, _ in h5py_dataset_iterator(hdf_file):
        yield path


filename = "sample.h5"
# df = pd.DataFrame()
data = {}
data_tem = {}
df = None
max_size = 0
with h5py.File(filename, 'r') as f:
    for dset in traverse_datasets(f):
        print('Path:', dset)
        print('Shape:', f[dset].shape)
        print('Data type:', f[dset].dtype)
        max_size = max(f[dset].shape[0], max_size)
        if len(f[dset].shape) == 1:
            data[dset] = np.array(f[dset])
        else:
            data_tem[dset] = np.array(f[dset])
# max_size = max([data[key].shape[0] for key in data])

df = pd.DataFrame.from_dict(data)
for key in data_tem:
    df_tem = pd.DataFrame({key: list(data_tem[key])})
    # print(df_tem.head(20))
    df = pd.merge(
        df,
        df_tem,
        how='left',
        left_index=True,  # Merge on both indexes, since right only has 0...
        right_index=True  # all the other rows will be NaN
    )

print('create dataframe from h5 file')
print(df.head(20))
csv_file_name = 'sample.csv'
df.to_pickle(csv_file_name)  # where to save it, usually as a .pkl

df_new = pd.read_pickle(csv_file_name)
print('read file content')
print(df_new.head(20))
