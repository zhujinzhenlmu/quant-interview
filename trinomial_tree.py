# Dynamic Trinomial Tree
import numpy as np
import sys


def trinomial_price(S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, n: int)->float:
    assert PutCall == 'call' or PutCall == 'put', 'The type should be put or call, intead of '+PutCall
    b = r - q
    dt = T / n
    u = np.exp(v*np.sqrt(2.0*dt))
    d = 1.0 / u
    pu = np.power((np.exp(0.5*b*dt) - np.exp(-v*np.sqrt(0.5*dt))) /
                  (np.exp(v*np.sqrt(0.5*dt)) - np.exp(-v*np.sqrt(0.5*dt))), 2)
    pd = ((np.exp(v*np.sqrt(0.5*dt)) - np.exp(0.5*b*dt)) /
          (np.exp(v*np.sqrt(0.5*dt)) - np.exp(-v*np.sqrt(0.5*dt))))**2
    pm = 1.0 - pu - pd
    z = 1 if PutCall == 'call' else -1
    OptionValue = np.zeros(2*(n+1))
    for i in range(2*n+1):
        OptionValue[i] = max(z*(S*np.power(u, (i - n)) - K), 0.0)
    # This code is like Haug but different because I don't use double max
    for j in range(n-1, -1, -1):
        for i in range(2*j+1):
            if OpStyle == 'E':
                OptionValue[i] = np.exp(-r*dt)*((pu*OptionValue[i + 2]) +
                                                (pm*OptionValue[i + 1]) + (pd*OptionValue[i]))
            else:
                OptionValue[i] = max(z*(S*np.power(u, (i - j)) - K), np.exp(-r*dt)*(
                    (pu*OptionValue[i + 2]) + (pm*OptionValue[i + 1]) + (pd*OptionValue[i])))
     # Return the option price
    return OptionValue[0]


class TrinomialTree:
    def __init__(self, max_inter: int = 1000, min_inter: int = 100, step_size: int = 10, precision: float = 0.0001):
        self.max_inter = max_inter
        self.min_inter = min_inter
        self.step_size = step_size
        self.precision = precision

    def calculate_value(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str)->float:
        before_cal = trinomial_price(
            S, K, T, r, v, q,  PutCall, OpStyle, self.min_inter)
        for n in range(self.min_inter+self.step_size, self.max_inter+1, self.step_size):
            tem = trinomial_price(S, K, T, r, v, q, PutCall, OpStyle, n)
            if np.abs(tem-before_cal)/before_cal >= self.precision:
                before_cal = tem
            else:
                return tem
        return before_cal

    def tt_call(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return self.calculate_value(S, K, T, r, v, q, 'call', 'E')

    def tt_put(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return self.calculate_value(S, K, T, r, v, q, 'put', 'E')

    def derivatives(self, params: list, indexes: list, val=None)->float:
        if len(indexes) == 0:
            return self.calculate_value(*params)
        params_cp = params.copy()
        index = indexes[-1]
        if abs(params[index]) < 0.0001:
            params[index] = 0.0001
        val = val if val != None else self.derivatives(params, indexes[:-1])
        before_cal = val + val*params[index]
        for n in range(self.min_inter, self.max_inter+1, self.step_size):
            params_cp[index] = params[index] + params[index]*1./n
            val_tem = self.derivatives(params_cp, indexes[:-1])
            der_tem = (val_tem-val)/(params_cp[index]-params[index])
            if np.abs(der_tem-before_cal)/der_tem >= self.precision:
                before_cal = der_tem
            else:
                return der_tem
        print('get the maximum min_inter at index %i' % index)
        return before_cal

    def Delta(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, val: float = None)->float:
        return self.derivatives([S, K, T, r, v, q, PutCall, OpStyle], [0], val)

    def Rho(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, val: float = None)->float:
        return self.derivatives([S, K, T, r, v, q, PutCall, OpStyle], [3], val)

    def Vega(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, val: float = None)->float:
        return self.derivatives([S, K, T, r, v, q, PutCall, OpStyle], [4], val)

    def Epsilon(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, val: float = None)->float:
        return self.derivatives([S, K, T, r, v, q, PutCall, OpStyle], [5], val)

    def Theta(self, S: float, K: float, T: float, r: float, v: float, q: float, PutCall: str, OpStyle: str, val: float = None)->float:
        return -self.derivatives([S, K, T, r, v, q, PutCall, OpStyle], [2], val)

    def Lambda(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        delta = self.Delta(S, K, T, r, v, q, PutCall, OpStyle, val=val)
        val = self.calculate_value(S, K, T, r, v, q, PutCall, OpStyle)
        return delta*S/val

    def Gamma(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [0, 0]
        return self.derivatives(params, indexes, val)

    def Vanna(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [0, 4]
        return self.derivatives(params, indexes, val)

    def Charm(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [2, 0]
        return -self.derivatives(params, indexes, val)

    def Vomma(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [4, 4]
        return self.derivatives(params, indexes, val)

    def Veta(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [4, 2]
        return -self.derivatives(params, indexes, val)

    def Vera(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [4, 3]
        return -self.derivatives(params, indexes, val)

    def Speed(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [0, 0, 0]
        return self.derivatives(params, indexes, val)

    def Zomma(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [0, 0, 4]
        return self.derivatives(params, indexes, val)

    def Color(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [0, 0, 2]
        return self.derivatives(params, indexes, val)

    def Ultima(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str, val: float = None)->float:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        indexes = [4, 4, 4]
        return self.derivatives(params, indexes, val)

    def print_greeks(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, OpStyle: str)->None:
        params = [S, K, T, r, v, q, PutCall, OpStyle]
        val = self.calculate_value(*params)
        print('Delta: %.4f' % self.Delta(*params, val=val))
        print('Rho: %.4f' % self.Rho(*params, val=val))
        print('Vega: %.4f' % self.Vega(*params, val=val))
        print('Epsilon: %.4f' % self.Epsilon(*params, val=val))
        print('Theta: %.4f' % self.Theta(*params, val=val))
        print('Lambda: %.4f' % self.Lambda(*params, val=val))
        print('Higer orders')

        print('Gamma: %.4f' % self.Gamma(*params))
        print('Vanna: %.4f' % self.Vanna(*params))
        print('Charm: %.4f' % self.Charm(*params))
        print('Vomma: %.4f' % self.Vomma(*params))
        print('Veta: %.4f' % self.Gamma(*params))
        print('Vera: %.4f' % self.Vera(*params))
        print('Speed: %.4f' % self.Speed(*params))
        print('Zomma: %.4f' % self.Zomma(*params))
        print('Color: %.4f' % self.Color(*params))
        print('Ultima: %.4f' % self.Ultima(*params))


if __name__ == '__main__':
    S = 100.0         # Spot Price
    K = 100.0      # Strike Price
    r = 0.03      # risk Free Rate
    q = 0.07      # Dividend
    v = 0.20                        # Volatility
    T = 3       # Years to maturity
    PutCall = 'call'
    OpStyle = 'E'
    n = 500
    print("The Trinomial Output %.10f" %
          trinomial_price(S, K, T, r, v, q, PutCall, OpStyle, n))
    tt = TrinomialTree(max_inter=3000)
    value = tt.calculate_value(S, K, T, r, v, q, PutCall, OpStyle)
    print('calculated value is %.4f' % value)
    print('calculated greeks are')

    print(tt.greeks(S, K, T, r, v, q, PutCall, OpStyle))
