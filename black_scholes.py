

from math import log, sqrt, pi, exp
from scipy.stats import norm
import numpy as np


class BlackScholes:
    def __init__(self):
        pass

    def d1(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return (np.log(S/K)+(r - q + v**2/2.)*T)/(v*np.sqrt(T))

    def d2(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return self.d1(S, K, T, r, v, q)-v*np.sqrt(T)

    def bs_call(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return S*np.exp(-q*T)*norm.cdf(self.d1(S, K, T, r, v, q))-K*np.exp(-r*T)*norm.cdf(self.d2(S, K, T, r, v, q))

    def bs_put(self, S: float, K: float, T: float, r: float, v: float, q: float)->float:
        return K*np.exp(-r*T)-S*np.exp(-q*T)+self.bs_call(S, K, T, r, v, q)

    def calculate_value(self, S: float, K: float, T: float, r: float, v: float, q: float = 0., option_type: str = 'call', option_style: str = 'E')->float:
        assert option_type == 'call' or option_type == 'put', 'option_type should be call or put'
        return self.bs_call(S, K, T, r, v, q) if option_type == 'call' else self.bs_put(S, K, T, r, v, q)

    def Delta(self, S: float, K: float, T: float, r: float, v: float, q: float, option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return np.exp(-q*T)*norm.cdf(d1) if option_type == 'call' else - \
            np.exp(-q*T)*norm.cdf(-d1)

    def Rho(self, S: float, K: float, T: float, r: float, v: float, q: float, option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return K*T*np.exp(-r*T)*norm.cdf(d2) if option_type == 'call' else - \
            K*T*np.exp(-r*T) * norm.cdf(-d2)

    def Vega(self, S: float, K: float, T: float, r: float, v: float, q: float, option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return S*T_sqrt*norm.pdf(d1)*np.exp(-q*T)

    def Epsilon(self, S: float, K: float, T: float, r: float, v: float, q: float, option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        if option_type == 'call':
            return -S*T*np.exp(-q*T)*norm.cdf(d1)
        else:
            return -S*T*np.exp(-q*T)*norm.cdf(-d1)

    def Theta(self, S: float, K: float, T: float, r: float, v: float, q: float, option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        if option_type == 'call':
            return -S*v*np.exp(-q*T) * norm.pdf(d1)/(2*T_sqrt)-r*K*np.exp(-r*T)*norm.cdf(d2)+q*S * \
                np.exp(-q*T)*norm.cdf(d1)
        else:
            return -S*v*np.exp(-q*T) * norm.pdf(-d1)/(2*T_sqrt)+r*K*np.exp(-r*T)*norm.cdf(-d2)-q*S * \
                np.exp(-q*T)*norm.cdf(-d1)

    def Lambda(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        delta = self.Delta(S, K, T, r, v, q, option_type, option_style)
        val = self.calculate_value(S, K, T, r, v, q, option_type, option_style)
        return delta*S/val

    def Gamma(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return norm.pdf(d1)/(S*v*T_sqrt)*np.exp(-q*T)

    def Vanna(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return -np.exp(-q*T)*norm.pdf(d1)*d2/v

    def Charm(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        if option_type == 'call':
            return q*np.exp(-q*T)*norm.cdf(d1)-np.exp(-q*T)*norm.pdf(d1)*(2*(r-q)*T-d2*v*T_sqrt)/(2*T*v*T_sqrt)
        else:
            return -q*np.exp(-q*T)*norm.cdf(-d1)-np.exp(-q*T)*norm.pdf(d1)*(2*(r-q)*T-d2*v*T_sqrt)/(2*T*v*T_sqrt)

    def Vomma(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return S*np.exp(-q*T)*norm.pdf(d1)*T_sqrt*d1*d2/v

    def Veta(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return -S*np.exp(-q*T)*norm.pdf(d1)*T_sqrt*(q+(r-q)*d1/(v*T_sqrt)-(1+d1*d2)/(2*T))

    def Vera(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        if option_type == 'call':
            return -25*np.sqrt(2)*np.exp(-(v**2+2*r)**2/(8*v**2))*(v**2+2*r)/(v**2*np.sqrt(np.pi))

    def Speed(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        gamma = self.Gamma(S, K, T, r, v, q, option_type, option_style)
        return -gamma/S*(d1/(v*T_sqrt)+1)

    def Zomma(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        gamma = self.Gamma(S, K, T, r, v, q, option_type, option_style)
        return gamma * (d1*d2-1)/v

    def Color(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        return -np.exp(-q*T)*norm.pdf(d1)/(2*S*T*v*T_sqrt)*(2*q*T+1+(2*(r-q)*T-d2*v*T_sqrt)*d1/(v*T_sqrt))

    def Ultima(self, S: float, K: float, T: float, r: float, v: float, q: float,  option_type: str, option_style: str)->float:
        T_sqrt, d1, d2 = self.greeks_helper(S, K, T, r, v, q)
        vega = self.Vega(S, K, T, r, v, q, option_type, option_style)
        return -vega/(v**2)*(d1*d2*(1-d1*d2)+d1**2+d2**2)

    def greeks_helper(self, S: float, K: float, T: float, r: float, v: float, q)->tuple:
        return np.sqrt(T), self.d1(S, K, T, r, v, q), self.d2(S, K, T, r, v, q)

    def print_greeks(self, S: float, K: float, T: float, r: float, v: float, q: float,  PutCall: str, option_style: str)->None:
        params = [S, K, T, r, v, q, PutCall, option_style]
        print('Delta: %.4f' % self.Delta(*params))
        print('Rho: %.4f' % self.Rho(*params))
        print('Vega: %.4f' % self.Vega(*params))
        print('Epsilon: %.4f' % self.Epsilon(*params))
        print('Theta: %.4f' % self.Theta(*params))
        print('Lambda: %.4f' % self.Lambda(*params))
        print('Gamma: %.4f' % self.Gamma(*params))
        print('Vanna: %.4f' % self.Vanna(*params))
        print('Charm: %.4f' % self.Charm(*params))
        print('Vomma: %.4f' % self.Vomma(*params))
        print('Veta: %.4f' % self.Gamma(*params))
        print('Vera: %.4f' % self.Vera(*params))
        print('Speed: %.4f' % self.Speed(*params))
        print('Zomma: %.4f' % self.Zomma(*params))
        print('Color: %.4f' % self.Color(*params))
        print('Ultima: %.4f' % self.Ultima(*params))

    def greeks(self, S: float, K: float, T: float, r: float, v: float,  q: float, option_type: str = 'call')->dict:
        assert option_type == 'call' or option_type == 'put', 'option_type should be call or put'
        val = self.calculate_value(S, K, T, r, v, q, option_type)
        T_sqrt = np.sqrt(T)
        d1 = self.d1(S, K, T, r, v, q)
        d2 = self.d2(S, K, T, r, v, q)
        Delta = np.exp(-q*T)*norm.cdf(d1) if option_type == 'call' else - \
            np.exp(-q*T)*norm.cdf(-d1)
        Gamma = norm.pdf(d1)/(S*v*T_sqrt)*np.exp(-q*T)
        Theta = None
        if option_type == 'call':
            Theta = -S*v*np.exp(-q*T) * norm.pdf(d1)/(2*T_sqrt)-r*K*np.exp(-r*T)*norm.cdf(d2)+q*S * \
                np.exp(-q*T)*norm.cdf(d1)
        else:
            Theta = -S*v*np.exp(-q*T) * norm.pdf(-d1)/(2*T_sqrt)+r*K*np.exp(-r*T)*norm.cdf(-d2)-q*S * \
                np.exp(-q*T)*norm.cdf(-d1)
        Vega = S*T_sqrt*norm.pdf(d1)*np.exp(-q*T)
        Rho = K*T*np.exp(-r*T)*norm.cdf(d2) if option_type == 'call' else - \
            K*T*np.exp(-r*T) * norm.cdf(-d2)
        Epsilon = None
        if option_type == 'call':
            Epsilon = -S*T*np.exp(-q*T)*norm.cdf(d1)
        else:
            Epsilon = -S*T*np.exp(-q*T)*norm.cdf(-d1)
        # return Delta, Gamma, Theta, Vega, Rho
        return {
            'Delta': Delta,
            'Vega': Vega,
            # minus derivative for r
            'Theta': Theta,
            'Rho': Rho,  # minus derivative for r
            'Lambda': Delta*S/val,
            'Epsilon': Epsilon
        }
# Implied Volatility using bisection


if __name__ == '__main__':
    from black_scholes import BlackScholes

    bs = BlackScholes()
    print(bs.greeks(100, 100, 0.4, 0.005, 0.06, 0))
    print(bs.greeks(100, 100, 0.4, 0.005, 0.06, 0, option_type='put'))
    print(bs.implied_vol('put', 5.57, 100, 100, 1, 0.05, 0))
