
from socket import socket, AF_INET, SOCK_DGRAM
from time import time, ctime, sleep
import sys
import os


class HBClient:
    def __init__(self, server_ip: str, hb_port: int, client_name: str = '', beat_wait: int = 10):
        self.server_ip = server_ip
        self.hb_port = hb_port
        self.beat_wait = beat_wait
        self.client_name = client_name
        assert client_name == '' or len(
            client_name) == 7, 'The client_name with length 7 or empty'

    def run(self):
        hbsocket = socket(AF_INET, SOCK_DGRAM)
        print("Client sending to IP %s , port %d" %
              (self.server_ip, self.hb_port))
        print("\n*** Press Ctrl-C to terminate ***\n")
        while 1:
            message = 'Default' if self.client_name == '' else self.client_name
            try:
                result = hbsocket.connect_ex((self.server_ip, self.hb_port))
                hbsocket.sendto(
                    message.encode(), (self.server_ip, self.hb_port))
                data, addr = hbsocket.recvfrom(1024)
                print(data.decode())
                print('server OK at ' + ctime(time()))
            except:
                print('can not connect to server at ' + ctime(time()))
            # print(tempVal)
            sleep(self.beat_wait)


SERVERIP = '127.0.0.1'    # local host, just for testing
HBPORT = 43278            # an arbitrary UDP port
BEATWAIT = 10             # number of seconds between heartbeats

if __name__ == '__main__':
    server_ip = '127.0.0.1'
    hb_port = 43278
    client_name = ''
    if len(sys.argv) > 1:
        server_ip = sys.argv[1]
    if len(sys.argv) > 2:
        hb_port = int(sys.argv[2])
    if len(sys.argv) > 3:
        client_name = sys.argv[3]
    hb_client = HBClient(server_ip, hb_port,
                         client_name=client_name, beat_wait=BEATWAIT)
    hb_client.run()
