import sys
from time import time, ctime, sleep
from threading import Lock, Thread, Event
from socket import socket, gethostbyname, AF_INET, SOCK_DGRAM
HBPORT = 43278
CHECKWAIT = 30


class BeatDict:
    #"Manage heartbeat dictionary"

    def __init__(self):
        self.beat_dict = {}
        # if __debug__:
        #     self.beat_dict['127.0.0.1'] = time()
        self.dict_lock = Lock()

    def __repr__(self)->str:
        list_info = ''
        self.dict_lock.acquire()
        for key in self.beat_dict.keys():
            list_info = "%sID info: %s - Last time: %s\n" % (
                list_info, key, ctime(self.beat_dict[key]))
        self.dict_lock.release()
        return list_info

    def update(self, entry: str)->None:
        "Create or update a dictionary entry"
        self.dict_lock.acquire()
        self.beat_dict[entry] = time()
        self.dict_lock.release()

    def extract_silent(self, howPast: float)->list:
        "Returns a list of entries older than howPast"
        silent = []
        when = time() - howPast
        self.dict_lock.acquire()
        for key in self.beat_dict.keys():
            if self.beat_dict[key] < when:
                silent.append(key)
        self.dict_lock.release()
        return silent


class HBServer(Thread):
    "Receive UDP packets, log them in heartbeat dictionary"

    def __init__(self, go_on_event, update_dict_func, port):
        Thread.__init__(self)
        self.go_on_event = go_on_event
        self.update_dict_func = update_dict_func
        self.port = port
        self.rec_socket = socket(AF_INET, SOCK_DGRAM)
        self.rec_socket.bind(('', port))

    def __repr__(self):
        return "Heartbeat Server on port: %d\n" % self.port

    def run(self):
        while self.go_on_event.isSet():
            if __debug__:
                print("Waiting to receive...")
            data, addr = self.rec_socket.recvfrom(7)
            key = str(addr[0])+':'+str(addr[1])
            if data.decode() != 'Default':
                self.update_dict_func(key)
                key = data.decode()
            if __debug__:
                print("Received packet from " + key)
            self.update_dict_func(key)


def main():
    #"Listen to the heartbeats and detect inactive clients"
    global HBPORT, CHECKWAIT
    if len(sys.argv) > 1:
        HBPORT = sys.argv[1]
    if len(sys.argv) > 2:
        CHECKWAIT = sys.argv[2]

    beatRecgo_on_event = Event()
    beatRecgo_on_event.set()
    beat_dictObject = BeatDict()
    hbServer = HBServer(
        beatRecgo_on_event, beat_dictObject.update, HBPORT)
    if __debug__:
        print(hbServer)
    hbServer.start()
    print("PyHeartBeat server listening on port %d" % HBPORT)
    print("\n*** Press Ctrl-C to stop ***\n")
    while 1:
        try:
            if __debug__:
                print("Beat Dictionary")
                print(beat_dictObject)
            silent = beat_dictObject.extract_silent(CHECKWAIT)
            if silent:
                print("Silent clients")
                print(silent)
            sleep(CHECKWAIT)
        except KeyboardInterrupt:
            print("Exiting.")
            beatRecgo_on_event.clear()
            hbServer.join()


if __name__ == '__main__':
    main()
