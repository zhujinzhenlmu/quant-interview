
import matplotlib
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from scipy.stats import norm, pearsonr, spearmanr, kendalltau
font = {'family': 'normal',
        # 'weight': 'bold',
        'size': 22}
matplotlib.rc('font', **font)


class Question1:
    def __init__(self, file_name):
        self.content = pd.read_csv(file_name).dropna()
        self.content.sort_index(inplace=True)
        self.window = 252
        self.content['Daily Return-Portfolio A'] = self.content['Portfolio A'].pct_change()
        self.content['Daily Return-Portfolio B'] = self.content['Portfolio B'].pct_change()

    def annulized_return(self, label: str, period: int = 365)->float:
        total_dates = len(self.content.index)
        first_value = self.content[label].iloc[0]
        last_value = self.content[label].iloc[-1]
        return (1 + (last_value-first_value)/first_value)**(period/total_dates)-1

    def max_drawdown(self, label: str)->float:
        roll_max = self.content[label].rolling(
            len(self.content.index), min_periods=1).max()
        daily_drawdown = self.content[label]/roll_max - 1.0
        return daily_drawdown.min()

    def sharpe_ratio(self, label: str, risk_free: float = 0.):
        return np.sqrt(self.window)*(self.content['Daily Return-'+label].mean() - risk_free) / self.content['Daily Return-'+label].std()

    def sortino_ratios(self, label: str, risk_free: float = 0.):
        self.content['downside_return'+label] = 0.
        self.content.loc[self.content['Daily Return-' + label] < risk_free,
                         'downside_return'+label] = self.content['Daily Return-' + label]**2
        expected_return = self.content['Daily Return-' + label].mean()
        down_stdev = np.sqrt(self.content['downside_return'+label].mean())
        return np.sqrt(self.window)*(expected_return - risk_free)/down_stdev

    def value_at_risk(self, label: str, prob: float = 0.95, initial_value: float = 1.)->float:
        mu = (self.content['Daily Return-'+label].mean()+1) * initial_value
        sigma = self.content['Daily Return-' +
                             label].std()*initial_value
        return initial_value - norm.ppf(1-prob, mu, sigma)

    def calculate_rs(self, daily_return: bool = True)->dict:
        prefix = 'Daily Return-' if daily_return else ''
        x = np.array(self.content[prefix + 'Portfolio A'].tolist()[1:])
        y = np.array(self.content[prefix + 'Portfolio B'].tolist()[1:])
        return {
            'pearsonr': pearsonr(x, y)[0],
            'spearmanr': spearmanr(x, y)[0],
            'kendalltau': kendalltau(x, y)[0]
        }

    def plot_correlations(self):
        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(16, 8))
        v_a, v_b = self.content['Portfolio A'].to_numpy(
        ), self.content['Portfolio B'].to_numpy()
        axes[0].plot(v_a,
                     v_b, 'o', label='datas')
        from scipy.stats import linregress
        slope, intercept, r, p, se = linregress(v_a, v_b)
        x_val = np.arange(min(v_a), max(v_a), (max(v_a)-min(v_a))/100)
        y_val = x_val*slope+intercept
        axes[0].plot(x_val, y_val, label='fit')
        axes[0].set_xlabel('PA-value')
        axes[0].set_ylabel('PB-value')
        axes[0].legend()
        r_a, r_b = self.content['Daily Return-Portfolio A'].dropna().to_numpy(
        ), self.content['Daily Return-Portfolio B'].dropna().to_numpy()
        axes[1].plot(r_a,
                     r_b, 'o', label='datas')
        slope, intercept, r, p, se = linregress(r_a, r_b)
        x_r = np.arange(np.min(r_a), np.max(
            r_a), (np.max(r_a)-np.min(r_a))/100)
        y_r = x_r*slope+intercept
        axes[1].plot(x_r, y_r, label='fit')
        axes[1].set_xlabel('PA-daily return')
        axes[1].set_ylabel('PB-daily return')
        axes[1].legend()
        plt.tight_layout()
        plt.legend()
        plt.savefig('./plot_correlations.png')

    def r_factor_plot(self, window: int = 40, daily_return: bool = True):
        prefix = 'Daily Return-' if daily_return else ''
        portfolio_returns = self.content[[
            prefix+'Portfolio A', prefix+'Portfolio B']]
        self.content['Pearson-window%i' % window] = 0.
        self.content['Spearman-window%i' % window] = 0.
        self.content['Kendal-window%i' % window] = 0.
        for i in range(5, len(self.content.index)):
            x = portfolio_returns[prefix+'Portfolio A'][max(
                1, i-window):i].to_numpy()
            y = portfolio_returns[prefix+'Portfolio B'][max(
                1, i-window):i].to_numpy()
            pearson, spearman, kendall = pearsonr(x, y)[0], spearmanr(x, y)[
                0], kendalltau(x, y)[0]
            self.content['Pearson-window%i' % window].iloc[i] = pearson
            self.content['Spearman-window%i' % window].iloc[i] = spearman
            self.content['Kendal-window%i' % window].iloc[i] = kendall
        self.content['Pearson-window%i' %
                     window][:5] = self.content['Pearson-window%i' % window][6]
        self.content['Spearman-window%i' %
                     window][:5] = self.content['Spearman-window%i' % window][6]
        self.content['Kendal-window%i' %
                     window][:5] = self.content['Kendal-window%i' % window][6]
        # self.content['Portfolio A'].plot()
        # self.content['Portfolio B'].plot()
        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(12, 8))
        axes[0].plot(
            self.content[prefix + 'Portfolio A'].to_numpy(), label=('daily-return-' if daily_return else '')+'PA')
        axes[0].plot(
            self.content[prefix + 'Portfolio B'].to_numpy(), label=('daily-return-' if daily_return else '')+'PB')
        axes[0].set_ylabel(
            ('daily-return-' if daily_return else '') + 'portfolio')
        axes[0].legend()
        axes[1].plot(self.content['Pearson-window%i' %
                                  window].to_numpy(), label="Pearson")
        axes[1].plot(self.content['Spearman-window%i' %
                                  window].to_numpy(), label="Spearman")
        axes[1].plot(self.content['Kendal-window%i' %
                                  window].to_numpy(), label="Kendal")
        axes[1].set_ylabel('correlation ratio')
        axes[1].set_xlabel('trading days')
        plt.title('correlation-' +
                  'portfolio' if not daily_return else 'daily return')
        axes[1].legend()
        figure_name = './daily-correlation.png' if daily_return else './correlation.png'

        plt.savefig(figure_name)

    def run(self):
        # print('The annulized_return for two at a period of 365 days are')
        # print('%.2f for A' % (self.annulized_return('Portfolio A')*100))
        # print('%.2f for B' % (self.annulized_return('Portfolio B')*100))
        print('The annulized_return for two at a period of 252 trade days are')
        print('%.2f%% for A' %
              (self.annulized_return('Portfolio A', self.window)*100))
        print('%.2f%% for B' %
              (self.annulized_return('Portfolio B', self.window)*100))

        print('The voliaty for two at a period of 252 trade days are')
        print('%.2f for A' %
              (self.content['Portfolio A'].std()*np.sqrt(self.window)))
        print('%.2f for B' %
              (self.content['Portfolio B'].std()*np.sqrt(self.window)))

        print('The max drawdown for two at a period of 252 trade days are')
        print('%.2f %% for A' % (self.max_drawdown('Portfolio A')*100))
        print('%.2f %% for B' % (self.max_drawdown('Portfolio B')*100))

        print('The annual Sharpe ratios for two at a period of 252 trade days are')
        print('%.4f for A' % self.sharpe_ratio('Portfolio A'))
        print('%.4f for B' % self.sharpe_ratio('Portfolio B'))

        print('The annual Sortino ratios for two at a period of 252 trade days are')
        print('%.4f for A' % self.sortino_ratios('Portfolio A'))
        print('%.4f for B' % self.sortino_ratios('Portfolio B'))

        probility = 0.95
        print('The daily value at risk for two at a period of 252 trade days are')
        print('%.4f for A' % self.value_at_risk('Portfolio A', prob=probility))
        print('%.4f for B' % self.value_at_risk('Portfolio B', prob=probility))

        print('check correlation factors based on daily return:')
        correlations = self.calculate_rs()
        for key in correlations:
            print(key+':%.4f' % correlations[key])
        self.r_factor_plot()
        plt.show()
        print('check correlation factors based on portfolio:')
        correlations = self.calculate_rs(daily_return=False)
        for key in correlations:
            print(key+':%.4f' % correlations[key])
        self.r_factor_plot(daily_return=False)
        self.plot_correlations()
        plt.show()


if __name__ == '__main__':
    question1 = Question1('portfolio.csv')
    question1.run()
